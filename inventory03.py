"""inventory03.py

   author: rzfeeser "zach"
   contact: rzfeeser@email.example

   overview: Use the pyyaml library to read in an ansible inventory (in YAML format) and make a change before exporting."""

# imports go at the top
# python3 -m pip install pyyaml
import yaml


def main():
    """runtime code"""

    #### retrieve data ####

    with open("hosts2", "r") as yf:
        # we want to convert our open file into python data structs
        pyansdata = yaml.load(yf, Loader=yaml.FullLoader)  # the "Loader" prevents a warning when the code runs

    print(pyansdata)


    #### modifications ####

    # perform an update to our dictionary
    pyansdata['all']['children']['dbservers']['hosts']['zach.example.com'] = None

    # confirm transformation worked
    #print(pyansdata)

    #### Write out data ####

    # write out the results to "hosts2.updated" 
    with open("hosts2.updated", "w") as yf:
        yaml.dump(pyansdata, yf)   # this says take python data "pyansdata" and place it in open file object "yf"


main()
