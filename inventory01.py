"""inventory01.py
   author: RZFeeser
   contact: RZFeeser@example.com

   description: This script adds a new line to the end of the hosts file."""


def main():
    
    # open a file called hosts in append mode
    inventoryfile = open("hosts", "a")
    
    # add a string to the end of the file (a new host)
    inventoryfile.write("        four.example.com:\n")
    
    # add a string (an inventory var to the host)
    inventoryfile.write("          color:green")
    
    # save and exit the file
    inventoryfile.close()


main()
